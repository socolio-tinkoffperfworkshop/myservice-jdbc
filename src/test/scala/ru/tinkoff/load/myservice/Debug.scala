package ru.tinkoff.load.myservice

import ru.tinkoff.gatling.config.SimulationConfig._
import io.gatling.core.Predef._
import ru.tinkoff.load.myservice.scenarios._
import ru.tinkoff.load.jdbc.Predef._

class Debug extends Simulation {

  setUp(
    CommonScenario().inject(atOnceUsers(1))
  ).protocols(jdbcProtocol)
    .maxDuration(testDuration)

}
